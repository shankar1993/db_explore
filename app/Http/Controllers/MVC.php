<?php

namespace App\Http\Controllers;


use App\tasks;
use Carbon\Carbon;
//use Illuminate\Http\Request;
use Requests;
//use App\Http\Requests;
use Requests\CreateTaskRequest;
use App\Http\Controllers\Controller;

class MVC extends Controller {

    public function index() {
        return 'MVC';
//        $tasks = tasks::latest()->get();
//        $tasks = tasks::latest('published_at')->Itso()->get();
//        $tasks = tasks::latest('published_at')->published()->get();
//        $tasks = tasks::latest('published_at')->where('published_at', '>', Carbon::now())->get();
//        return $tasks;


        return view('admin.mvc', compact('tasks'));
    }
    
    public function index2(){
        return 'MVC2';
    }

    public function show($id) {

        $tasks = tasks::findOrFail($id);

//        dd($tasks->created_at->subDays(4)->diffForHumans());

        return view('admin.mvc2', compact('tasks'));
    }

    public function create() {

        return view('admin.mvc_create');
    }
    
    public function store(Requests\CreateTaskRequest $request) {
//    function store(Request $request) {
//        $this->validate($request,['title'=>'required|min:4','instruction'=>'required']);
        tasks::create($request->all());

        return redirect('MVC');
    }
    
    public function edit($id){
        $tasks = tasks::findOrFail($id);
        return view('admin/mvc_edit',compact('tasks'));
    }
    
    public function update($id, Request $request){
        $tasks = tasks::findOrFail($id);
        $tasks->update($request->all());
        
        return redirect('MVC');
    }

}
