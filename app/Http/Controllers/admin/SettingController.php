<?php

namespace App\Http\Controllers\admin;

use Request;
use App\Helper\OTF_DB;
use App\Http\Requests\OTF as OTF_Request;
use App\Http\Controllers\Controller;

class SettingController extends Controller
{

    public function myPanel()
    {
        return view('db_admin.dbSchema');
    }

    public function myNewPanel(OTF_Request $request)
    {
        $db_schema = $request->has('db_schema') ? $request->get('db_schema') : array();
        dd((new OTF_DB())->scanDatabase($db_schema));
    }
}
