<?php

namespace App\Http\Controllers;

class Home  extends Controller{
    function __construct() {
//        $this->data['name'] = 'shanar';
    }
    
    function index(){
        /*Passing Value to View*/
    
        //Method :1
//        $data['name'] = 'Shankar';
//        return view('admin/index',$this->data);

        //Method :2
//        return view('admin/index')->with([
//            'name' => $name
//        ]);
        
        //Method :3
//        $name = 'Shankar';
//        $age = 23;
//          return view('admin/home',compact('name','age'));

        $persons = [
          'Shankar','Bala','Kumar'  
        ];
        $name = 'Shankar';
        return view('admin/home',compact('persons','name'));
    }
}
