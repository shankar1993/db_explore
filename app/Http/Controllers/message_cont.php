<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;

use Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\messages;
use Requests\messageRequest;
use Auth\AuthController;
use Hash;

class message_cont extends Controller {

    public function __construct() {
        $this->middleware('auth',['except'=>'message/create']);
    }

    public function index() {
        $messages = messages::latest('created_at')->get();
        return view('messages/view', compact('messages'));
    }

//    101 Type:1 Retrive value by ID
//    public function show($id) {
//        $messages = messages::findOrFail($id);
//        return view('messages/show', compact('messages'));
//    }
//    101 Type:2 Retrive Value by Route Binding
    /*
     * @param Messages $messages
     * @return Response
     * 
     */
    public function show(Messages $messages) {

        $tags = $messages->tags->lists('name','id');

        return view('messages/show', compact('messages','tags'));
    }

    /* Show Create Page
     * @param 
     * @return Response
     * 
     */

    public function create() {
//       101 Type:1 Allow Page with privilege
//
//        if(\Auth::guest()){
//            return redirect('message');
//        }        
        $tags = \App\tags::lists('name', 'id');
        return view('messages/create', compact('tags'));
    }

    /*
     * To Store new Post to DB
     * @param Requests\messageRequest $request
     * @return Response
     */

    public function store(Requests\messageRequest $request) {
//        Auth::user();.
// 101 Type:1 To Create Post
//        $request = request::all();
//        $request['user_id'] = \Auth::id();    
//        messages::create($request);

// 101 Type:2 To Create Post
//        $message = new messages(request::all());
//        \Auth::user()->messages()->save($message);

// 101 Type:3 To Create Post
        $tags = $request->input('tags');
        $message = \Auth::User()->messages()->create(request::all());
        $message->tags()->attach($tags);

// 101 Type:1 Method to Send Session data
//        \Session::flash('flash_message', 'Message Created Successfully !');        
//      \Session::flash('alert-important',true);

// 101 Type:2 Method to Send Session data
        return redirect('message')->with([
                    'flash_message' => 'Message Created Successfully !'
        ]);
    }

    /* 101 Type:1 Edit Post by ID
    *
    * public function edit($id) {
    *    $message = messages::findOrFail($id);
    *    return view('messages/edit', compact('message'));
     }


    // 101 Type:2 Edit Post by Route Binding
    /* Show Edit Page
     * @param Messages $message
     * @return Response
     * 
     */
    public function edit(Messages $message) {
        $tags = \App\tags::lists('name', 'id');
        return view('messages/edit', compact('message','tags'));
    }

    /* Update the Post Contents.
     * @param Requests\messageRequest $request, Messages $message
     * @return Response
     *
     * @function sync()
     *  Used to sync the changes to update by replacing data.
     */

    public function update(Requests\messageRequest $request, Messages $message) {

//      $message = messages::findOrFail($id);
        $message->update(Request::all());
        $message->tags()->sync($request->input('tags'));
        return redirect('message');



    }

    /* Test Route Binding Module
     * @param Messages $news
     * @return Response
     * 
     */

    public function bar(Messages $news) {
        var_dump($news);
    }

    public function test() {
        $a = 'New';
        echo $a;
//       phpinfo();
    }

    public function tags($id){
        $tag = \App\tags::find($id);
        $messages = $tag->messages;
//        dd($messages);
        $tag_name = \App\tags::find($id);


        return view('messages/view', compact('messages','tag_name'));
    }

}
