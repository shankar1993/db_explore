<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Demo {

    /**
     * Handle an incoming request with some parameter level redirections.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if ($request->is('message/create') && $request->has('foo')) {

            return redirect('message');
        }elseif($request->is('message') && $request->has('bar')){
            return redirect('message/create');
        }
        return $next($request);
    }

}
