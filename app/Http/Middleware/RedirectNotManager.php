<?php

namespace App\Http\Middleware;

use Closure;

class RedirectNotManager
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$request->user()->isTeamManager()){
            return redirect('message');
        }
        
        return $next($request);
    }
}
