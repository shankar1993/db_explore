<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class OTF extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'db_schema.name' => 'required',
            'db_schema.host' => 'required',
            'db_schema.username' => 'required',
            'db_schema.database' => 'required'
        ];
    }
}
