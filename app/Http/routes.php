<?php

/*
  |--------------------------------------------------------------------------
  | Routes File
  |--------------------------------------------------------------------------
  |
  | Here is where you will register all of the routes in an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */



/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | This route group applies the "web" middleware group to every route
  | it contains. The "web" middleware group is defined in your HTTP
  | kernel and includes session state, CSRF protection, and more.
  |
 */

Route::group(['middleware' => ['web']], function () {
    Route::get('/', function () {
        return view('home');
    });
    Route::resource('message', 'message_cont');
//    Route::get('MVC', ['middleware' => 'auth', 'uses' => 'MVC@index']);
//    Route::get('MVC2', ['middleware' => 'auth', 'uses' => 'MVC@index2']);
//    Route::get('MVC3', ['middleware' => 'auth', function() {
//            return 'Authorized Page';
//        }]);

    Route::auth();
    Route::controllers([
        'auth' => 'Auth\AuthController',
        'password' => 'Auth\PasswordController'
    ]);

    Route::get('tags/{id}','message_cont@tags');


//    101 Testing Routes

//    Route::get('foo', ['middleware' => 'manager', function() {
//            return 'Authorized Page';
//        }]);
//    Route::get('bar', function() {
//        return '123';
//    });
    
//    Route::get('message/foo/{news}','message_cont@bar');
    
    Route::get('test','message_cont@test');

    /** DB Explorer */

    Route::get('DB','admin\SettingController@myPanel');
    Route::post('DB','admin\SettingController@myNewPanel');
    /***************************************** */
});

