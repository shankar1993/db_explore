<?php

namespace App\Providers;

use App\Helper\OTF;
use Illuminate\Support\ServiceProvider;

class OTFServiceProvider extends ServiceProvider
{
    protected $namespace = 'App\Http\Controllers';
    /**
     * Bootstrap the application services.
     *
     * @return void
     */

    public function boot(Router $router)
    {
//        $router->bind('OTF', function()
//        {
//            return new OTF();
//        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
