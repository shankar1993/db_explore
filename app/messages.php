<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class messages extends Model
{
    
    protected $table = 'messages';
    
    protected $fillable = [
        'title',
        'message',
        'published_on',
        'user_id'
    ];
    
/*-------------------------------------------------------*
*   To Get Records that belongs to User.
*--------------------------------------------------------*
*
*@Param 
*@Return Response
*
*/   
    public function user(){
        return $this->belongsTo('App\User');
    }
   
/*-------------------------------------------------------*
*   To Get Records that belongs to Messages.
*--------------------------------------------------------*
*
*@Param
*@Return
*
*/
    function tags(){
       return $this->belongsToMany('App\tags', "messages_tags", "messages_id", "tags_id")->withTimestamps();
    }
    
    
/*-------------------------------------------------------*
*   To Set "Published_on" parameter format
*--------------------------------------------------------*
*
*@Param $date
*@Return Response
*
*/
    public function setPublishedOnAttribute($date){
        $this->attributes['published_on'] = Carbon::createFromFormat('Y-m-d',$date);
    }    
    
}

















