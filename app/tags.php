<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tags extends Model {
    /* -------------------------------------------------------*
     *       To Get Messages with Tags.
     * --------------------------------------------------------*
     *
     * @Param
     * @Return Response
     *
     */

    public function messages() {
        return $this->belongsToMany('App\messages');
    }

}
