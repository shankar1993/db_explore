@extends('app')

@section('title') Hello @stop

@section('content')

@if($name!='')
<h4>Welcome {{$name}}</h4>
<ul>
    @foreach($persons as $per)
    <li>{{$per}}</li>
    @endforeach
</ul>
@else
<h4>Hello Guest</h4>

@endif

@stop