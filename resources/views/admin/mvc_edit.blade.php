@extends('app')

@section('content')
<h1>Edit : {{ $tasks->title }}</h1>
<hr>
{!! Form::model($tasks,['method'=>'PATCH', 'url'=>'Tasks/'.$tasks->id]) !!}
<div class="form-group">
    {!! Form::label('title','Title :') !!} 
    {!! Form::text('title',null,['class'=>'form-control','placeholder'=>'Enter the Title']) !!}
</div>
<div class="form-group">
    {!! Form::label('instruction','Instruction :') !!} 
    {!! Form::textarea('instruction',null,['class'=>'form-control','placeholder'=>'Your Instructions']) !!}
</div>
<div class="form-group">
    {!! Form::label('published_at','Publish On :') !!} 
    {!! Form::input('date','published_at',date('Y-m-d'),['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::submit('Create',['class'=>'btn btn-primary form-control']) !!}
</div>
{!! Form::close() !!}

    @if($errors->any())

<ul class="alert alert-danger">
       @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
       @endforeach
</ul>
    @endif
@stop
