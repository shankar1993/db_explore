<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Laravel 5 | @yield('title')</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css" rel="stylesheet"/>

</head>
<body>
<div class="container">
    @if(Session::has('flash_message'))
        <div class="alert alert-success {{ Session::has('alert-important') ? 'alert-important':'' }} ">
            @if(session::has('alert-important'))
                <button type='button' class="close" data-dismiss='alert' aria-hidden='true'>&times;</button>
            @endif
            {{ Session::get('flash_message') }}
        </div>
    @endif
    @yield('content')
</div>
<script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.min.js"></script>
<script>
    $('div .alert').not('.alert-important').delay(2000).slideUp();
</script>
@yield('footer')
</body>
</html>











