@extends('layouts.app')

@section('title')
    DB Schema
@stop

@section('content')
    <div class="container">
        <div class="row">
            <h1 class="text-muted">Config Database</h1>
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Make My Admin Panel
                    </div>
                    <div class="panel-body">
                        <div class="col-md-10">
                            <form method="post" action="DB">
                                {{ csrf_field()}}
                                {{--123123--}}
                                {{--{!! form::open(['url'=>'DB']) !!}--}}
                                {{--<div class=""></div>--}}
                                {{--{!! form::close !!}--}}
                                <div class="form-group">
                                    <label>DB Schema : </label>
                                    <select class="form-control" name="db_schema[name]">
                                        <option value="mysql">MySQL</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Host : </label>
                                    <input type="text" class="form-control" name="db_schema[host]">
                                </div>
                                <div class="form-group">
                                    <label>Username : </label>
                                    <input type="text" class="form-control" name="db_schema[username]">
                                </div>
                                <div class="form-group">
                                    <label>Password : </label>
                                    <input type="text" class="form-control" name="db_schema[password]">
                                </div>
                                <div class="form-group">
                                    <label>Database : </label>
                                    <input type="text" class="form-control" name="db_schema[database]">
                                </div>
                                <input type="submit" value="Create" class="btn btn-primary">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <ul class="alert alert-danger">
        @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
@stop

@section('footer')

@stop