@extends('app')

@section('content')

    <h2>Create Message</h2>
    {!! Form::open(['url'=>'message']) !!}
    <div class="form-group">
        {!! Form::label('title','Title : ') !!}
        {!! Form::text('title',null,['class'=>'form-control', 'placeholder'=>'Enter Title']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('message','Message : ') !!}
        {!! Form::textarea('message',null,['class'=>'form-control', 'placeholder'=>'Enter Message']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('published_on','Published On : ') !!}
        {!! Form::text('published_on',null,['class'=>'form-control', 'placeholder'=>'Date to Publish']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('tags','Tags : ') !!}
        {!! Form::select('tags[]', $tags, null, ['id'=>'tag_list','class'=>'form-control', 'multiple']) !!}

    </div>

    <div class="form-group">
        {!! Form::submit('Create',['class'=>'form-control btn btn-primary']) !!}
    </div>
    <h4><a href="{!! url('/test') !!}">TEST</a></h4>
    {!! Form::close() !!}

    @include('errors.list')

@section('footer')
    <script>
        $('#tag_list').select2({
            placeholder: 'Choose Your Tags',
            tags: true,
            ajax:{
                dataType: 'json',
                url: 'api/tags',
                delay: 250,
                data: function(params){
                    return{
                        q:params.term
                    }
                },
                processResults: function(data){
                    return {results:data}
                }
            }
        });
    </script>

@endsection

@stop