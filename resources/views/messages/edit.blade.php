@extends('app')

@section('content')

<h2>Edit Message</h2>
{!! Form::model($message,['method'=>'PATCH','url'=>'message/'.$message->id]) !!}
<div class="form-group">
    {!! Form::label('title','Title') !!}
    {!! Form::text('title',null,['class'=>'form-control', 'placeholder'=>'Enter Title']) !!}
</div>
<div class="form-group">
    {!! Form::label('message','Message') !!}
    {!! Form::textarea('message',null,['class'=>'form-control', 'placeholder'=>'Enter Message']) !!}
</div>
<div class="form-group">
    {!! Form::label('published_on','Published On') !!}
    {!! Form::text('published_on',date('Y-m-d'),['class'=>'form-control', 'placeholder'=>'Date to Publish']) !!}
</div>
<div class="form-group">
    {!! Form::label('Tags','Tags : ') !!}
    {!! Form::select('tags[]',$tags, null, ['id'=>'tag_list','class'=>'form-control', 'multiple']) !!}
</div>
<div class="form-group">
    {!! Form::submit('Update Message',['class'=>'form-control btn btn-primary']) !!}
</div>

{!! Form::close() !!}

@include('errors.list')
@section('footer')
    <script>
        $('#tag_list').select2({
            placeholder: 'Choose Your Tags'
        });
    </script>

@endsection

@stop