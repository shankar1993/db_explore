@extends('app')

@section('content')
    <h1>{{ $messages->id .' . '. $messages->title }}</h1>
    <h4>{{ $messages->message }}</h4>
    <h5>{{ $messages->published_on }}</h5>
    @if(!$tags->isEmpty())
        <b>TAGS : </b><br>
        <ul>
            @foreach($tags as $id=>$val)
                <li><a href="http://{{ env('HOST') }}/tags/{{$id}}" title="{{ $val }}">{{ $val }}</a></li>
            @endforeach
        </ul>
    @endif

    <h5><a href="http://{{ env('HOST') }}/message/{{$messages->id}}/edit" title="{{ $messages->title }}">Edit</a></h5>
@stop

@section('title')
    Show
@stop