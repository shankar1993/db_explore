@extends('app')

@section('content')
    @if(isset($tag_name->name))
        <h1>{{$tag_name->name}}</h1>
    @else
        <h1>Messages</h1>
    @endif
    <hr>
    @foreach($messages as $message)
        <h1>{{$message->id. ' . '. $message->title }}</h1>
        <h4>{{ $message->message }}</h4>
        <h5><a href="http://{{ env('HOST') }}/message/{{$message->id}}"
               title="{{ $message->title }}">{{ $message->title }}</a></h5>
    @endforeach
@stop

@section('title')
    Show
@stop